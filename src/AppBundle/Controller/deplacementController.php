<?php
namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use AppBundle\Entity\deplacement;

class deplacementController extends Controller {
/**
* @Route("/create-deplacement")
*/
public function createAction(Request $request) {

  $deplacement = new deplacement();
  $form = $this->createFormBuilder($deplacement)
   
    ->add('nom', TextType::class)
    ->add('prenom', TextType::class)
    ->add('email', TextType::class)
    ->add('datedepart', DateType::class)
    ->add('dateretour', DateType::class)
    ->add('heuredepart', DateType::class)
    ->add('heureretour', DateType::class)
    ->add('moyentransport', TextType::class)
    ->add('idville', TextType::class)
    ->add('save', SubmitType::class, array('label' => 'New deplacement'))
    ->getForm();

  $form->handleRequest($request);

  if ($form->isSubmitted()) {

    $deplacement = $form->getData();

    $em = $this->getDoctrine()->getManager();
    $em->persist($deplacement);
    $em->flush();

    return $this->redirect('/view-deplacement/' . $deplacement->getId());

  }

  return $this->render(
    'deplacement/edit.html.twig',
    array('form' => $form->createView())
    );

}
}
?>
